package com.activity.sportive.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tableau_sportif")
public class ActiviterSportive implements Serializable
{
  private static final long serialVersionUID = 1L;
  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-d");
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private int idActivity;
  private float duree;
  private float distance;
  private boolean natation;
  private LocalDate date;
  
  public ActiviterSportive()
  {
  }
  
  public ActiviterSportive(LocalDate dateDebut, boolean natation)
  {  
    
    System.out.println("=>"+dateDebut.toString());
    this.idActivity = 0;
    this.distance = 0;
    this.duree = 0;
    this.date = LocalDate.parse(dateDebut.toString(),DATE_FORMAT);
    this.natation = natation;
  }
  
  public ActiviterSportive(int idActivity, int duree, int distance, boolean natation, LocalDate date)
  {
    this.idActivity = idActivity;
    this.distance = distance;
    this.natation = natation;
    this.duree = duree;
    this.date = date;
  }
  
  public int getidActivity()
  {
    return this.idActivity; 
  }
  
  public void setidActivity(int idActivity)
  {
    this.idActivity = idActivity;  
  }

  public float getDistance()
  {
    return distance;
  }

  public void setDistance(float distance)
  {
    this.distance = distance;
  }

  public boolean isNatation()
  {
    return natation;
  }

  public void setNatation(boolean natation)
  {
    this.natation = natation;
  }
  
  public void setDuree(float duree)
  {
    this.duree = duree;
  }
  
  public float getDuree()
  {
    return this.duree;
  }
  
  public void setDate(LocalDate date)
  {
    this.date = date;
  }
  
  public String getDate()
  {
    return this.date.toString();
  }

  public static DateTimeFormatter getDateFormat()
  {
    return DATE_FORMAT;
  }
  
  @Override
  public String toString()
  {
    return "[" + this.getidActivity()
               + ", " + (this.isNatation() ? "Natation" : "Course") 
               + ", " + this.getDate()
               + ", " + this.getDuree()
               + ", " + this.getDistance()
               + ", " + (this.getDuree()>0?((this.getDistance()/this.getDuree())):"0")
              + "]";
  }

  
/*  
  @RequestMapping(value="/getData", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getAjson()
  {
    String pattern = "{\"idActivity\":\"%s\",\"Activitée\":\"%s\",\"Date début\":\"%s\",\"Distance\":\"%S\",\"Duréée\":\"s\",\"Vitesse moyenne\":\"%s\"}";
    return String.format(pattern,questions.get(numPanneau).getIdPanneau());
  }*/
}
