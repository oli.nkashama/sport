package com.activity.sportive.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.sportive.models.ActiviterSportive;
import com.activity.sportive.repositorys.ActiviterSportiveRepository;

@Controller
public class PageController
{
  private final ActiviterSportiveRepository actRepository;
  private ActiviterSportive lactivite;
  private String patReponse = "{\"Message\":\"%s\",\"Status\":\"%s\"}";
  private String patData = " {\"idActivité\":\"%s\",\"isNatation\":\"%s\",\"Date\":\"%s\",\"Durée\":\"%s\",\"Distance\":\"%s\",\"Vitesse\":\"%s\"}";
  @Autowired
  public PageController(ActiviterSportiveRepository aRepository)
  {
    actRepository = aRepository;
  }
  
  @GetMapping("/sport")
  @ResponseBody
  public String home()
  {
    return "Ici le home";
  }
 
  @RequestMapping(value="/sport/debut", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String sartActivity(@RequestParam boolean natation)
  {
    try
    {
      lactivite = new ActiviterSportive(LocalDate.now(), natation);
      return String.format(patReponse, "","200");
    }
    catch(Exception e)
    {
      return String.format(patReponse, e,"500");
    }
  }
  @RequestMapping(value="/sport/fin", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String endActivite(@RequestParam int duree, @RequestParam int distance)
  {
    try
    {
      lactivite.setDuree(duree);
      lactivite.setDistance(distance);    
      actRepository.save(lactivite);
      return String.format(patReponse, "","200");
    }
    catch(Exception e)
    {
      return String.format(patReponse, e,"500");
    }
  }
  
  @RequestMapping(value="/sport/find/all", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getAll()
  {
    try
    {
      return buildData(null,0);
    }
    catch(Exception e)
    {
      return String.format(patReponse, e,"500");
    }
  }
  
  @RequestMapping(value="/sport/find/month", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getMois(@RequestParam String date)
  {
    try
    {
      return buildData(Optional.of(LocalDate.parse(date, ActiviterSportive.getDateFormat())),1);      
    }
    catch(Exception e)
    {
      return String.format(patReponse, e.toString().replace(":","").replace("\"", ""),"500");
    }
  }
  
  @RequestMapping(value="/sport/find/all/swimming", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getAllNatation()
  {
    try
    {
      return buildData(null,2);      
    }
    catch(Exception e)
    {
      return String.format(patReponse, e.toString().replace(":","").replace("\"", ""),"500");
    }
  }
  
  @RequestMapping(value="/sport/find/all/footing", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getAllCourse()
  {
    try
    {
      return buildData(null,3);      
    }
    catch(Exception e)
    {
      return String.format(patReponse, e.toString().replace(":","").replace("\"", ""),"500");
    }
  }
  
  @RequestMapping(value="/sport/find/swimming/date", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getPiscineDate(@RequestParam String date)
  {
    List<ActiviterSportive> listeNatation = actRepository.findByNatation(true);
    listeNatation.removeIf((natation)->Period.between(LocalDate.parse(date, ActiviterSportive.getDateFormat()), LocalDate.parse(natation.getDate(), ActiviterSportive.getDateFormat())).isZero()==false);
    return buildData(listeNatation);
  }
  
  @RequestMapping(value="/sport/find/footing/date", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public String getCourseDate(@RequestParam String date)
  {
    List<ActiviterSportive> listeCourse = actRepository.findByNatation(false);
    listeCourse.removeIf((course)->Period.between(LocalDate.parse(date, ActiviterSportive.getDateFormat()), LocalDate.parse(course.getDate(), ActiviterSportive.getDateFormat())).isZero()==false);
    return buildData(listeCourse);
  }
  
  private String buildData(Optional<LocalDate> date, int fonction)
  {
    String retour="{\"Activity\":[";
    int compteur=0;
    for(ActiviterSportive activity : (fonction==0?actRepository.findAll():fonction==1?actRepository.findByDate(date.get()):fonction==2?actRepository.findByNatation(true):actRepository.findByNatation(false)))
    {        
      retour += String.format(patData, activity.getidActivity(), activity.isNatation(), activity.getDate(), activity.getDuree(), activity.getDistance(), (activity.getDistance()/activity.getDuree())) + ",";
      compteur++;
    }
    retour = retour.length()>15?retour.substring(0,retour.length()-1):retour;
    retour +="],\"Count\":"+compteur+",\"Status\":\"200\"}";
    return retour;
  }
  
  private String buildData(List<ActiviterSportive> data)
  {
    String retour="{\"Activity\":[";
    int compteur=0;
    for(ActiviterSportive activity :data)
    {        
      retour += String.format(patData, activity.getidActivity(), activity.isNatation(), activity.getDate(), activity.getDuree(), activity.getDistance(), (activity.getDistance()/activity.getDuree())) + ",";
      compteur++;
    }
    retour = retour.length()>15?retour.substring(0,retour.length()-1):retour;
    retour +="],\"Count\":"+compteur+",\"Status\":\"200\"}";
    return retour;
  }
}