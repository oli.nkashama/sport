package com.activity.sportive.repositorys;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.activity.sportive.models.ActiviterSportive;
@Repository
public interface ActiviterSportiveRepository extends CrudRepository<ActiviterSportive,LocalDateTime>
{
  List<ActiviterSportive> findAll();
  List<ActiviterSportive> findByDate(LocalDate date);
  List<ActiviterSportive> findByNatation(boolean natation);
}
